from constructs import Construct
from aws_cdk import Stack, aws_dynamodb as _dynamodb

class DynamoDbStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        USER_TABLE = _dynamodb.Table(
            self,
            "USER_TABLE",
            partition_key=_dynamodb.Attribute(
                name="account_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="User"
        )

        USER_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="account_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="email",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="account_id"
        )

        PROFILE_TABLE = _dynamodb.Table(
            self,
            "PROFILE_TABLE",
            partition_key=_dynamodb.Attribute(
                name="profile_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="Profile"
        )

        PROFILE_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="profile_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="account_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="profile_id-account_id-index"
        )

        GAME_TABLE = _dynamodb.Table(
            self,
            "GAME_TABLE",
            partition_key=_dynamodb.Attribute(
                name="game_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="Game"
        )        

        COLLECTION_TABLE = _dynamodb.Table(
            self,
            "COLLECTION_TABLE",
            partition_key=_dynamodb.Attribute(
                name="collection_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="Collection"
        )
        
        COLLECTION_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="collection_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="account_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="collection_id-account_id-index"
        )

        REVIEW_AND_RATING_TABLE = _dynamodb.Table(
            self,
            "REVIEW_AND_RATING_TABLE",
            partition_key=_dynamodb.Attribute(
                name="review_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="ReviewAndRating"
        )

        REVIEW_AND_RATING_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="review_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="game_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="review_id-game_id-index"
        )

        REVIEW_AND_RATING_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="review_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="account_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="review_id-account_id-index"
        )

        HOUSE_RULE_TABLE = _dynamodb.Table(
            self,
            "HOUSE_RULE_TABLE",
            partition_key=_dynamodb.Attribute(
                name="rule_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            table_name="HouseRule"
        )

        HOUSE_RULE_TABLE.add_global_secondary_index(
            partition_key=_dynamodb.Attribute(
                name="rule_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            sort_key=_dynamodb.Attribute(
                name="game_id",
                type=_dynamodb.AttributeType.STRING,
            ),
            index_name="rule_id-game_id-index"
        )