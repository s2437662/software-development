#!/usr/bin/env python3

import aws_cdk as cdk

from Lambda.lambda_stack import LambdaStack
from DynamoDB.dynamodb_stack import DynamoDbStack


app = cdk.App()
LambdaStack(app, "SD")
DynamoDbStack(app, "SD-DB")

app.synth()
