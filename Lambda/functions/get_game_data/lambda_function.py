import json
import boto3
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
GAME_TABLE = dynamodb.Table("Game")
REVIEW_AND_RATING_TABLE = dynamodb.Table("ReviewAndRating")


def lambda_handler(event, context):
    try:
        query_string = event.get("queryStringParameters")
        game_id = query_string.get("game_id")

        game_data = query_game_table(game_id)
        game_reviews_and_ratings = get_game_ratings_and_reviews(game_id)

        game_data["reviews_and_ratings"] = game_reviews_and_ratings

        response = {
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(game_data),
        }

        return response

    except Exception as ex:
        print(ex)
        return {
            "statusCode": 400,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }


def query_game_table(game_id):
    game = GAME_TABLE.get_item(Key={"game_id": game_id})

    return game.get("Item") if game else None


def get_game_ratings_and_reviews(game_id):
    ratings_and_review = REVIEW_AND_RATING_TABLE.scan(
        FilterExpression=Attr("game_id").eq(game_id)
    )

    return ratings_and_review.get("Items") if ratings_and_review else None
