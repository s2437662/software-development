"""
This lambda function uses the BoardGameGeek API to pull a list of games
and create new record in the Game and ReviewAndRating tables. A total of 100 
games will be created. This lambda function should only be called once to set
up the game data in DynamoDB tables.
"""

import os
import boto3
import requests
import uuid
from xml.etree import ElementTree

dynamodb = boto3.resource('dynamodb')
GAME_TABLE = dynamodb.Table('Game')
REVIEW_AND_RATING_TABLE = dynamodb.Table('ReviewAndRating')
BGG_API_ROOT_PATH = os.environ.get('BGG_API_ROOT_PATH')

def lambda_handler(event, context):
    try:
        
        for i in range(1, 101):
            response = requests.get(BGG_API_ROOT_PATH + "thing?id=" + str(i) + "&type=boardgame&stats=1&comments=1")
            tree = ElementTree.fromstring(response.content)
            tree_item = tree.find(".//item")
            if tree_item:
                game_title = tree.find(".//item/name").attrib.get("value")
                game_description = tree.find(".//item/description").text
                thumbnail = tree.find(".//item/thumbnail").text
                game_image = tree.find(".//item/image").text
                year_published = tree.find(".//item/yearpublished").attrib.get("value")
                min_players = tree.find(".//item/minplayers").attrib.get("value")
                max_players = tree.find(".//item/maxplayers").attrib.get("value")
                average_ratings = tree.find(".//item/statistics/ratings/average").attrib.get("value")
                
                game_id = str(uuid.uuid4())
                
                j = 1
                for comment in tree.iter('comment'):
                    if j <= 10:
                        rating = comment.attrib.get('rating')
                        review = comment.attrib.get('value')
                        username = comment.attrib.get('username')
                        create_ratings_and_reviews_record(game_id, rating, review, username)
                        j+=1
                    else:
                        break
        
                create_game_record(game_id, game_title, game_description, game_image, thumbnail, year_published, min_players, max_players, average_ratings)
        return {'status': True, 'message': 'New game created successfully'}  
        
    except Exception as ex:
        print(ex)
        return {'status': False, 'message': 'The server encountered an unexpected condition that prevent function from executing!'}

def create_game_record(game_id, game_title, game_description, game_image, thumbnail, year_published, min_players, max_players, average_ratings):
    game_record = {
        "game_id": game_id,
        "game_title": game_title,
        "game_description": game_description,
        "game_image": game_image,
        "thumbnail": thumbnail,
        "year_published": year_published,
        "min_players": min_players,
        "max_players": max_players,
        "average_ratings": average_ratings
    }
    
    GAME_TABLE.put_item(Item=game_record)

def create_ratings_and_reviews_record(game_id, rating, review, username):
    rating_and_review = {
        "review_id": str(uuid.uuid4()),
        "game_id": game_id,
        "review": review,
        "rating": rating,
        "author": username
    }
    REVIEW_AND_RATING_TABLE.put_item(Item=rating_and_review)