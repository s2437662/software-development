"""
This function returns a list of user created game collections.
"""
import json
import boto3
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
COLLECTION_TABLE = dynamodb.Table("Collection")


def lambda_handler(event, context):
    try:
        query_string = event.get("queryStringParameters")
        account_id = query_string.get("account_id")

        user_collections = get_user_collections(account_id)

        response = {
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(user_collections),
        }
        print(response)

        return response
    except Exception as ex:
        print(ex)
        return {
            "statusCode": 400,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }


def get_user_collections(account_id):
    user_collections = COLLECTION_TABLE.scan(
        FilterExpression=Attr("account_id").eq(account_id)
    )

    return user_collections.get("Items") if user_collections else None
