import boto3 
dynamodb = boto3.resource('dynamodb')
PROFILE_TABLE = dynamodb.Table('Profile')

def update_profile(key,val,profile_id):
    """
    update one attribute of the profile 
    """
    PROFILE_TABLE.update_item(
    Key={"profile_id":profile_id},
    UpdateExpression="set {} = :n".format(key),
    ExpressionAttributeValues={":n": val},
    ReturnValues="UPDATED_NEW"
    )

def lambda_handler(event, context):
    profile_id = event["profile_id"]
    try: 
        for k,v in event:
            update_profile(k,v,profile_id)
        # sign in the cognito 
        return {"status":False,"message":"profile updated!"}
    except:
        return {"status":True,"message":"there is an error when updating the profile."}