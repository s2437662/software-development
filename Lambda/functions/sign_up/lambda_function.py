"""
This method should take in users' email, password, 
region first_name and last_name as input. Calling this function 
should create a new user type Account record, a new Profile 
record in the database and register a new user in Cognito. The method should 
return true if there is no error during the creation process and false otherwise
"""
import os
import boto3
from uuid import uuid4
from datetime import datetime

dynamodb = boto3.resource("dynamodb")
cognito_client = boto3.client("cognito-idp", region_name="eu-west-2")

USER_TABLE = dynamodb.Table("User")
PROFILE_TABLE = dynamodb.Table("Profile")
COGNITO_CLIENT_ID = os.getenv("COGNITO_CLIENT_ID")


def lambda_handler(event, context):
    """
    The main function for user sign up
    """
    try:
        event_body = event.get("body")

        user_account_id = None
        if event_body:
            cognito_sign_up(event_body)
            user_account_id = dynamodb_sign_up(event_body)

        return {
            "statusCode": 200,
            "body": {
                "message": "Account successfully created !",
                "account_id": user_account_id,
            },
        }
    except Exception as ex:
        return {
            "statusCode": 500,
            "message": "The server encountered an unexpected condition that prevent function from executing!",
        }


def dynamodb_sign_up(event_body):
    """
    creates a user and account instance in the database.
    """

    now = datetime.now()
    now_str = now.strftime("%Y-%m-%d %H:%M:%S")  # get the date of signing-up

    account_id = str(uuid4())
    username = event_body.get("first_name") + event_body.get("last_name")

    new_item_user = {
        "account_id": account_id,
        "username": username,
        "account_type": "user",
        "email": event_body.get("email"),
        "join_date": now_str,
    }

    new_item_profile = {
        "profile_id": str(uuid4()),
        "account_id": account_id,
        "display_name": "",
        "profile_picture": "",
        "region": event_body.get("region"),
    }

    # create user in cognito
    USER_TABLE.put_item(Item=new_item_user)
    PROFILE_TABLE.put_item(Item=new_item_profile)
    return account_id


def cognito_sign_up(event_body):
    """
    creates a user and account instance in cognito for verification purposes.
    """

    cognito_client.sign_up(
        ClientId=COGNITO_CLIENT_ID,
        Username=event_body.get("email"),
        Password=event_body.get("password"),
        UserAttributes=[{"Name": "email", "Value": event_body.get("email")}],
    )
