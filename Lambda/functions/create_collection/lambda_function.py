"""
This method should take in account_id, a collection_title and a description about the collection.
Calling this lambda function should create a new record in the Collection table
for the user
"""
import boto3
from uuid import uuid4

dynamodb = boto3.resource("dynamodb")
COLLECTION_TABLE = dynamodb.Table("Collection")


def lambda_handler(event, context):
    try:
        event_body = event.get("body")

        account_id = event_body.get("account_id")
        collection_title = event_body.get("collection_title")
        collection_description = event_body.get("description")

        create_new_collection(account_id, collection_title, collection_description)

        return {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            },
            "body": "Collection created successfully!",
        }
    except Exception as ex:
        print(ex)
        return {
            "statusCode": 400,
            "message": "The server encountered an unexpected condition that prevent function from executing!",
        }


def create_new_collection(account_id, collection_title, collection_description):
    new_collection = {
        "collection_id": str(uuid4()),
        "account_id": account_id,
        "collection_title": collection_title,
        "description": collection_description,
        "game_collection": [],
    }

    response = COLLECTION_TABLE.put_item(Item=new_collection)

    return response
