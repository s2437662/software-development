import json
import boto3
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
COLLECTION_TABLE = dynamodb.Table("Collection")
GAME_TABLE = dynamodb.Table("Game")


def lambda_handler(event, context):
    try:
        query_string = event.get("queryStringParameters")
        collection_id = query_string.get("collection_id")

        collection_data = get_collection_info(collection_id)
        game_id_list = collection_data.get("game_collection")
        game_data_list = get_game_in_user_collection(game_id_list)
        collection_data["game_data_list"] = game_data_list

        response = {
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(collection_data),
        }

        return response

    except Exception as ex:
        print(ex)
        return {
            "statusCode": 400,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }


def get_collection_info(collection_id):
    collection = COLLECTION_TABLE.get_item(Key={"collection_id": collection_id})

    return collection.get("Item") if collection else None


def get_game_in_user_collection(game_id_list):
    game_data_list = []
    for game_id in game_id_list:
        game = GAME_TABLE.get_item(Key={"game_id": game_id})

        if game.get("Item"):
            game_details = game.get("Item")
            game_data = {
                "thumbnail": game_details.get("thumbnail"),
                "gameTitle": game_details.get("game_title"),
                "gameDescription": game_details.get("game_description"),
                "gameId": game_details.get("game_id"),
            }

            game_data_list.append(game_data)
        else:
            continue

    return game_data_list
