"""
This method should take in users' email and verification code as input.
Calling this function should verify the verification code with the record 
in Amazon Cognito and confirm the user status. The method should 
return true if the user entered correct verification code and false otherwise.
"""
import os
import boto3

COGNITO_CLIENT = boto3.client("cognito-idp", region_name="eu-west-2")
COGNITO_CLIENT_ID = os.getenv("COGNITO_CLIENT_ID")


def lambda_handler(event, context):
    try:
        event_body = event.get("body")

        response = COGNITO_CLIENT.confirm_sign_up(
            ClientId=COGNITO_CLIENT_ID,
            Username=event_body.get("email"),
            ConfirmationCode=event_body.get("verification_code"),
        )
        return {"statusCode": 200, "message": "Account verified!"}

    except Exception as ex:
        return {
            "status": 500,
            "message": "The server encountered an unexpected condition that prevent function from executing!",
        }
