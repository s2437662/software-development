import json
import boto3

dynamodb = boto3.resource("dynamodb")
GAME_TABLE = dynamodb.Table("Game")


def lambda_handler(event, context):
    try:
        response_body = GAME_TABLE.scan()

        game_data = response_body.get("Items")

        while "LastEvaluatedKey" in response_body:
            new_response_body = GAME_TABLE.scan(
                ExclusiveStartKey=response.get("LastEvaluatedKey")
            )
            game_data.extend(new_response_body.get("Items"))

        response = {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": "*",
            },
            "body": json.dumps(game_data),
        }

        return response

    except Exception as ex:
        print(ex)
        return {
            "statusCode": 400,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }
