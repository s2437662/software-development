"""
This function takes in a game_id and a collection_id to
allow users to add a selected game to a selected collection.
"""
import boto3
from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource("dynamodb")
COLLECTION_TABLE = dynamodb.Table("Collection")


def lambda_handler(event, context):
    try:
        event_body = event.get("body")
        game_id = event_body.get("game_id")
        collection_id = event_body.get("collection_id")

        user_selected_collection = get_user_collections(collection_id)
        game_in_collection = user_selected_collection.get("game_collection")

        response = {
            "statusCode": 500,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": "Something went wrong please try again later!",
        }

        if game_id not in game_in_collection:
            game_in_collection.append(game_id)
            update_collection(collection_id, game_in_collection)
            response = {
                "statusCode": 200,
                "headers": {"Access-Control-Allow-Origin": "*"},
                "body": "Added game to collection!",
            }

        else:
            response = {
                "statusCode": 400,
                "headers": {"Access-Control-Allow-Origin": "*"},
                "body": "Game already in the collection!",
            }

        return response

    except Exception as ex:
        print(ex)
        return {
            "statusCode": 500,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }


def get_user_collections(collection_id):
    collection = COLLECTION_TABLE.query(
        KeyConditionExpression=Key("collection_id").eq(collection_id)
    )

    return collection.get("Items")[0] if collection else None


def update_collection(collection_id, new_game_list):
    response = COLLECTION_TABLE.update_item(
        Key={
            "collection_id": collection_id,
        },
        UpdateExpression="set game_collection = :gc",
        ExpressionAttributeValues={
            ":gc": new_game_list,
        },
        ReturnValues="UPDATED_NEW",
    )

    return response
