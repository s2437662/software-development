"""
This method should take in users' email and password, 
and verify them against the data in aws cognito and provide access
if they match the record
"""
import os
import boto3
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
cognito_client = boto3.client("cognito-idp", region_name="eu-west-2")

USER_TABLE = dynamodb.Table("User")
PROFILE_TABLE = dynamodb.Table("Profile")
COGNITO_CLIENT_ID = os.getenv("COGNITO_CLIENT_ID")


def lambda_handler(event, context):
    """
    The main function for user sign up
    """
    try:
        event_body = event.get("body")

        if event_body:
            email = event_body.get("email")
            password = event_body.get("password")

            auth_token = sign_in_user(email, password)
            user = get_account_id(email)
            account_id = user[0].get("account_id")

        response = {
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": {"auth_token": auth_token, "account_id": account_id},
        }
        return response

    except Exception as ex:
        print(ex)
        return {
            "statusCode": 500,
            "message": "The server encountered an unexpected condition that prevent function from executing",
        }


def sign_in_user(email, password):
    """
    Authenticate user with provided email and password
    """

    # Initiating the Authentication,
    response = cognito_client.initiate_auth(
        ClientId=COGNITO_CLIENT_ID,
        AuthFlow="USER_PASSWORD_AUTH",
        AuthParameters={"USERNAME": email, "PASSWORD": password},
    )

    return response


def get_account_id(email):
    user = USER_TABLE.scan(FilterExpression=Attr("email").eq(email))

    return user.get("Items") if user else None
