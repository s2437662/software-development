import os
from dotenv import load_dotenv
from constructs import Construct
from aws_cdk import (
    Duration,
    Stack,
    aws_lambda as _lambda,
    aws_iam as _iam,
    aws_apigateway as agw,
)


class LambdaStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        load_dotenv()
        DEV_LAMBDA_EXECUTION_ROLE = os.getenv("DEV_LAMBDA_EXECUTION_ROLE")

        ## IAM Role that all lambda functions should assume
        lambda_execution_role = _iam.Role.from_role_arn(
            self, "LambdaBasicExecutionRole", DEV_LAMBDA_EXECUTION_ROLE, mutable=False
        )

        ## Defining REST API endpoint and create a stage called rabbit-gaming
        rest_api = agw.RestApi(
            self,
            "rest_api",
            rest_api_name="RabbitGamingApi",
            description="This is the API Gateway for the software development assesment coursework 3",
        )

        success_response_model = rest_api.add_model(
            "ResponseModel",
            content_type="application/json",
            model_name="SuccessResponseModel",
            schema=agw.JsonSchema(
                schema=agw.JsonSchemaVersion.DRAFT4,
                title="successResponse",
                type=agw.JsonSchemaType.OBJECT,
                properties={
                    "body": agw.JsonSchema(type=agw.JsonSchemaType.STRING),
                },
            ),
        )

        # We define the JSON Schema for the transformed error response
        error_response_model = rest_api.add_model(
            "ErrorResponseModel",
            content_type="application/json",
            model_name="ErrorResponseModel",
            schema=agw.JsonSchema(
                schema=agw.JsonSchemaVersion.DRAFT4,
                title="errorResponse",
                type=agw.JsonSchemaType.OBJECT,
                properties={"message": agw.JsonSchema(type=agw.JsonSchemaType.STRING)},
            ),
        )

        ## Lambda function definition

        generate_game_data = _lambda.Function(
            self,
            "generate_game_data",
            function_name="generate_game_data",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/generate_game_data"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(180),
            memory_size=128,
            environment={"BGG_API_ROOT_PATH": "	https://boardgamegeek.com/xmlapi2/"},
            description="Use BoardGameGeek API to pull a list of games and corresponding game details.",
            role=lambda_execution_role,
        )

        ## sign up lambda
        sign_up = _lambda.Function(
            self,
            "sign_up",
            function_name="sign_up",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/sign_up"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={"COGNITO_CLIENT_ID": "45298am8bqm1p4tsrasjmvmc8d"},
            description="Create account and profile in the database and register a user in Amazon Cognito.",
            role=lambda_execution_role,
        )

        sign_up_api = rest_api.root.add_resource("sign-up")
        sign_up_api.add_method(
            "POST",
            agw.LambdaIntegration(
                sign_up,
                proxy=False,
                integration_responses=[
                    agw.IntegrationResponse(
                        status_code="200",
                        response_parameters={
                            "method.response.header.Access-Control-Allow-Origin": "'*'"
                        },
                    ),
                    agw.IntegrationResponse(status_code="500"),
                ],
            ),
            method_responses=[
                agw.MethodResponse(
                    status_code="200",
                    response_models={"application/json": success_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
                agw.MethodResponse(
                    status_code="500",
                    response_models={"application/json": error_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
            ],
        )
        sign_up_api.add_cors_preflight(
            allow_origins=agw.Cors.ALL_ORIGINS, allow_methods=["POST"]
        )

        ## verify email lambda
        verify_user_email = _lambda.Function(
            self,
            "verify_user_email",
            function_name="verify_user_email",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/verify_user_email"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={"COGNITO_CLIENT_ID": "45298am8bqm1p4tsrasjmvmc8d"},
            description="Verify registered user identity with the email verification code.",
            role=lambda_execution_role,
        )

        verify_user_email_api = rest_api.root.add_resource("verify-email")
        verify_user_email_api.add_method(
            "POST",
            agw.LambdaIntegration(
                verify_user_email,
                proxy=False,
                integration_responses=[
                    agw.IntegrationResponse(
                        status_code="200",
                        response_parameters={
                            "method.response.header.Access-Control-Allow-Origin": "'*'"
                        },
                    ),
                    agw.IntegrationResponse(status_code="500"),
                ],
            ),
            method_responses=[
                agw.MethodResponse(
                    status_code="200",
                    response_models={"application/json": success_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
                agw.MethodResponse(
                    status_code="500",
                    response_models={"application/json": error_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
            ],
        )
        verify_user_email_api.add_cors_preflight(
            allow_origins=agw.Cors.ALL_ORIGINS, allow_methods=["POST"]
        )

        ## sign in lambda
        sign_in = _lambda.Function(
            self,
            "sign_in",
            function_name="sign_in",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/sign_in"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={"COGNITO_CLIENT_ID": "45298am8bqm1p4tsrasjmvmc8d"},
            description="sign in the user",
            role=lambda_execution_role,
        )

        sign_in_api = rest_api.root.add_resource("sign-in")
        sign_in_api.add_method(
            "POST",
            agw.LambdaIntegration(
                sign_in,
                proxy=False,
                integration_responses=[
                    agw.IntegrationResponse(
                        status_code="200",
                        response_parameters={
                            "method.response.header.Access-Control-Allow-Origin": "'*'"
                        },
                    ),
                    agw.IntegrationResponse(status_code="500"),
                ],
            ),
            method_responses=[
                agw.MethodResponse(
                    status_code="200",
                    response_models={"application/json": success_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
                agw.MethodResponse(
                    status_code="500",
                    response_models={"application/json": error_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
            ],
        )
        sign_in_api.add_cors_preflight(
            allow_origins=agw.Cors.ALL_ORIGINS, allow_methods=["POST"]
        )

        ## logout lambda
        logout = _lambda.Function(
            self,
            "logout",
            function_name="logout",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/logout"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="logout the user",
            role=lambda_execution_role,
        )

        ## reset password lambda
        reset_password = _lambda.Function(
            self,
            "reset_password",
            function_name="reset_password",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/reset_password"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="reset the password",
            role=lambda_execution_role,
        )

        ## update profile lambda
        update_profile = _lambda.Function(
            self,
            "update_profile",
            function_name="update_profile",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/update_profile"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="update the profile information",
            role=lambda_execution_role,
        )

        ## list game lambda
        list_game = _lambda.Function(
            self,
            "list_game",
            function_name="list_game",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/list_game"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="List all the games on the home page",
            role=lambda_execution_role,
        )

        list_game_api = rest_api.root.add_resource("list-game")
        list_game_api.add_method(
            "GET",
            agw.LambdaIntegration(
                list_game,
            ),
        )

        list_game_api.add_cors_preflight(
            allow_origins=["http://localhost:3000/"],
            allow_methods=["GET"],
            allow_headers=agw.Cors.DEFAULT_HEADERS,
        )

        ## get game data lambda
        get_game_data = _lambda.Function(
            self,
            "get_game_data",
            function_name="get_game_data",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/get_game_data"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="Get specific game data based on game_id input",
            role=lambda_execution_role,
        )

        get_game_data_api = rest_api.root.add_resource("get-game")
        get_game_data_api.add_method(
            "GET",
            agw.LambdaIntegration(
                get_game_data,
            ),
            request_parameters={"method.request.querystring.game_id": True},
        )

        ## create_collection api
        create_collection = _lambda.Function(
            self,
            "create_collection",
            function_name="create_collection",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/create_collection"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="Allow user to create new game collection",
            role=lambda_execution_role,
        )

        create_collection_api = rest_api.root.add_resource("create-collection")
        create_collection_api.add_method(
            "POST",
            agw.LambdaIntegration(
                create_collection,
                proxy=False,
                integration_responses=[
                    agw.IntegrationResponse(
                        status_code="200",
                        response_parameters={
                            "method.response.header.Access-Control-Allow-Origin": "'*'"
                        },
                    ),
                    agw.IntegrationResponse(status_code="400"),
                ],
            ),
            method_responses=[
                agw.MethodResponse(
                    status_code="200",
                    response_models={"application/json": success_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
                agw.MethodResponse(
                    status_code="500",
                    response_models={"application/json": error_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
            ],
        )
        create_collection_api.add_cors_preflight(
            allow_origins=agw.Cors.ALL_ORIGINS, allow_methods=["POST"]
        )

        ## get_collection_info lambda
        get_collection_info = _lambda.Function(
            self,
            "get_collection_info",
            function_name="get_collection_info",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/get_collection_info"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="Allow user to view the details of a collection.",
            role=lambda_execution_role,
        )

        get_collection_info_api = rest_api.root.add_resource("get-collection-info")
        get_collection_info_api.add_method(
            "GET",
            agw.LambdaIntegration(get_collection_info),
            request_parameters={"method.request.querystring.collection_id": True},
        )

        ## list_user_collection lambda
        list_user_collection = _lambda.Function(
            self,
            "list_user_collection",
            function_name="list_user_collection",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/list_user_collection"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="List out all the game collection that a user created.",
            role=lambda_execution_role,
        )

        list_user_collection_api = rest_api.root.add_resource("list-user-collection")
        list_user_collection_api.add_method(
            "GET",
            agw.LambdaIntegration(list_user_collection),
        )

        ## add_game_to_collection lambda
        add_game_to_collection = _lambda.Function(
            self,
            "add_game_to_collection",
            function_name="add_game_to_collection",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("Lambda/functions/add_game_to_collection"),
            handler="lambda_function.lambda_handler",
            timeout=Duration.seconds(30),
            memory_size=128,
            environment={},
            description="Allow user to add game to a collection.",
            role=lambda_execution_role,
        )

        add_game_to_collection_api = rest_api.root.add_resource("add-to-collection")
        add_game_to_collection_api.add_method(
            "POST",
            agw.LambdaIntegration(
                add_game_to_collection,
                proxy=False,
                integration_responses=[
                    agw.IntegrationResponse(
                        status_code="200",
                        response_parameters={
                            "method.response.header.Access-Control-Allow-Origin": "'*'"
                        },
                    ),
                    agw.IntegrationResponse(status_code="400"),
                ],
            ),
            method_responses=[
                agw.MethodResponse(
                    status_code="200",
                    response_models={"application/json": success_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
                agw.MethodResponse(
                    status_code="500",
                    response_models={"application/json": error_response_model},
                    response_parameters={
                        "method.response.header.Access-Control-Allow-Origin": True
                    },
                ),
            ],
        )
        add_game_to_collection_api.add_cors_preflight(
            allow_origins=agw.Cors.ALL_ORIGINS, allow_methods=["POST"]
        )
